import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LineComponent } from './line-chart.component';
import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts/release';
import { PieComponent } from './pie-chart.component';
import { RouterModule } from '@angular/router';
import { routes } from './../routes.config';

@NgModule({
  declarations: [
    AppComponent,
    LineComponent,
    PieComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    NgxChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
