import { AppComponent } from './app/app.component';
import { LineComponent } from './app/line-chart.component';
import { PieComponent } from './app/pie-chart.component';
export let routes = [
  {path: "", pathMatch: "full", component: LineComponent},
  {path: "line", component: LineComponent},
  {path: "pie", component: PieComponent},
]