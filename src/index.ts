import * as express from "express"
import * as http from "http"
import * as mongoose from "mongoose"

import {productRouter} from "./product/product.routes"
import {regionRouter} from "./region/region.routes"

let dbUri = "mongodb://localhost:27017/agri"
let dbConnection = mongoose.connect(dbUri).connection
dbConnection.on("error", () => {
  console.log(`Unable to connect to DB using the uri : ${dbUri} .`)
  process.exit(1)
})
dbConnection.on("connected", () => {
  startServer()
})


let startServer = () =>  {
  let app = express()
  let server = http.createServer(app)
  let port = process.env.PORT || 3000
  
  
  // define routes
  app.get("/", (req, res, next) => {
    return res.status(200).json("Hello !!!")
  })
  
  // region
  app.use("/api/regions", regionRouter)
  app.use("/api/products", productRouter)



  //
  server.listen(port, (err: Error) => {
    if (err) {
      console.log(err)
      process.exit(1)
    }
    console.log(`Application successfully started and listen port ${port} .`)
  })
}