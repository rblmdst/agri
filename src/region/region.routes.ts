import * as bodyparser from "body-parser"
import * as express from "express"

import { Region } from './region.model';

let regionRouter = express.Router()

regionRouter.get("/", async (req, res, next) => {
  try {
    let regions = await Region.find({})
    return res.status(200).json(regions)
  } catch (err) {
    console.log(err)
    return next(err)
  }
})

regionRouter.post("/", bodyparser.json(), async (req, res, next) => {
  try {
    let region = new Region(req.body)
    let createdRegion = await region.save()
    return res.status(201).json(createdRegion)
  } catch (err) {
    console.log(err)
    return next(err)
  }
})

export {regionRouter}