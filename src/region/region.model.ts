import * as mongoose from "mongoose"
let Schema = mongoose.Schema
let regionSchema = new Schema({
  name: {
    type: String
  }
})

export let Region = mongoose.model("Region", regionSchema)