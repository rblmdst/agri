import * as mongoose from "mongoose"
let Schema = mongoose.Schema
let productSchema = new Schema({
  name: {
    type: String
  }
})

export let Product = mongoose.model("Product", productSchema)