import * as bodyparser from "body-parser"
import * as express from "express"

import { Product } from './product.model';

let productRouter = express.Router()

productRouter.get("/", async (req, res, next) => {
  try {
    let products = await Product.find({})
    return res.status(200).json(products)
  } catch (err) {
    console.log(err)
    return next(err)
  }
})

productRouter.post("/", bodyparser.json(), async (req, res, next) => {
  try {
    let product = new Product(req.body)
    let createdProduct = await product.save()
    return res.status(201).json(createdProduct)
  } catch (err) {
    console.log(err)
    return next(err)
  }
})

export {productRouter}